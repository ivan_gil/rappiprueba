package com.rpi.tests;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rpi.api.Operacion;
import com.rpi.api.OperacionCubo;
import com.rpi.api.ResultadoOperacion;
import com.rpi.api.TipoOperacion;
import com.rpi.mvc.control.CuboServiceController;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { AppConfig.class })
public class RappiApplicationTests {

	@Autowired
	CuboServiceController cuboService;

	@Test
	public void pruebaCubo3DKO() {

		OperacionCubo operacionTotal = new OperacionCubo();
		ResultadoOperacion resultado = cuboService.traerResultadoOperaciones(operacionTotal);

		System.out.println(resultado);
		Assert.assertEquals(-1, resultado.getEstado());
	}

	@Test
	public void pruebaCubo3DOK() {

		OperacionCubo operacionTotal = new OperacionCubo();
		int numeroOperaciones = 2;
		operacionTotal.setNumeroConsultas(numeroOperaciones);
		operacionTotal.setNumeroTests(2);
		operacionTotal.setTamanoMatriz(40);
		operacionTotal.setOperaciones(new ArrayList<Operacion>());
		Operacion o1 = new Operacion();
		Operacion o2 = new Operacion();
		Operacion o3 = new Operacion();
		TipoOperacion update = new TipoOperacion();
		update.setId(1);
		TipoOperacion query = new TipoOperacion();
		query.setId(2);
		o1.setTipo(update);
		o1.setValorX(1);
		o1.setValorY(1);
		o1.setValorZ(1);
		o1.setValorX1(21);
		o3.setTipo(update);
		o3.setValorX(1);
		o3.setValorY(30);
		o3.setValorZ(22);
		o3.setValorX1(1);
		operacionTotal.getOperaciones().add(o1);
		o2.setTipo(query);
		o2.setValorX(1);
		o2.setValorY(1);
		o2.setValorZ(1);
		o2.setValorX1(1);
		o2.setValorY1(30);
		o2.setValorZ1(30);
		operacionTotal.getOperaciones().add(o3);
		operacionTotal.getOperaciones().add(o2);
		operacionTotal.getOperaciones().add(o3);
		ResultadoOperacion resultado = cuboService.traerResultadoOperaciones(operacionTotal);

		System.out.println(resultado);
		Assert.assertEquals(0, resultado.getEstado());
	}
}
