package com.rpi.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "cubo3D")
public class Cubo3D {

	private int[][][] bloques;

	public int[][][] getBloques() {
		return bloques;
	}

	public void setBloques(int[][][] bloques) {
		this.bloques = bloques;
	}

	public void setTamanoMatriz(int tamano) {
		this.bloques = new int[tamano + 1][tamano + 1][tamano + 1];
		for (int i = 0; i <= tamano; i++)
			for (int j = 0; j <= tamano; j++)
				for (int k = 0; k <= tamano; k++)
					this.bloques[i][j][k] = 0;
	}

}
