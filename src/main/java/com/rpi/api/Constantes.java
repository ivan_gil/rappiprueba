package com.rpi.api;

public interface Constantes {

	int NUMERO_MAX_TEST_PERMITIDO = 50;
	int NUMERO_MAX_TAMANO_PERMITIDO = 100;
	int NUMERO_MAX_CONSULTAS_PERMITIDO = 1000;
	int NUMERO_MAX_VALOR_PERMITIDO = 109;
	int NUMERO_MIN_VALOR_PERMITIDO = -109;
}
