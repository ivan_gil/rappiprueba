package com.rpi.api;

import java.io.Serializable;

/**
 *
 * @author Ivan Alberto Gil
 */

public class CfgField implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;

	private String caption;

	private String tooltip;

	private String prompt;

	private Object valor = "";

	public CfgField() {

	}

	public CfgField(Long id, String name, String caption, String tooltip, String prompt) {
		super();
		this.id = id;
		this.name = name;
		this.caption = caption;
		this.tooltip = tooltip;
		this.prompt = prompt;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public String getPrompt() {
		return prompt;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	public Object getValor() {
		return valor;
	}

	public void setValor(Object valor) {
		this.valor = valor;
	}

}
