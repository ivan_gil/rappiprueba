package com.rpi.api;

public class Operacion {
	private TipoOperacion tipo;
	private int valorX = 0;
	private int valorY = 0;
	private int valorZ = 0;
	private int valorX1 = 0;
	private int valorY1 = 0;
	private int valorZ1 = 0;

	public TipoOperacion getTipo() {
		return tipo;
	}

	public int getValorX() {
		return valorX;
	}

	public int getValorY() {
		return valorY;
	}

	public int getValorZ() {
		return valorZ;
	}

	public int getValorX1() {
		return valorX1;
	}

	public int getValorY1() {
		return valorY1;
	}

	public int getValorZ1() {
		return valorZ1;
	}

	public void setTipo(TipoOperacion tipo) {
		this.tipo = tipo;
	}

	public void setValorX(int valorX) {
		this.valorX = valorX;
	}

	public void setValorY(int valorY) {
		this.valorY = valorY;
	}

	public void setValorZ(int valorZ) {
		this.valorZ = valorZ;
	}

	public void setValorX1(int valorX1) {
		this.valorX1 = valorX1;
	}

	public void setValorY1(int valorY1) {
		this.valorY1 = valorY1;
	}

	public void setValorZ1(int valorZ1) {
		this.valorZ1 = valorZ1;
	}

	@Override
	public String toString() {
		return "Operacion [tipo=" + tipo + ", valorX=" + valorX + ", valorY=" + valorY + ", valorZ=" + valorZ
				+ ", valorX1=" + valorX1 + ", valorY1=" + valorY1 + ", valorZ1=" + valorZ1 + "]";
	}

}
