package com.rpi.api;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "operacionCubo")
public class OperacionCubo {

	private int numeroTests = 0;
	private int tamanoMatriz = 0;
	private int numeroConsultas = 0;
	private List<Operacion> operaciones;

	public int getNumeroTests() {
		return numeroTests;
	}

	public void setNumeroTests(int numeroTests) {
		this.numeroTests = numeroTests;
	}

	public int getTamanoMatriz() {
		return tamanoMatriz;
	}

	public void setTamanoMatriz(int tamanoMatriz) {
		this.tamanoMatriz = tamanoMatriz;
	}

	public int getNumeroConsultas() {
		return numeroConsultas;
	}

	public void setNumeroConsultas(int numeroConsultas) {
		this.numeroConsultas = numeroConsultas;
	}

	public List<Operacion> getOperaciones() {
		return operaciones;
	}

	public void setOperaciones(List<Operacion> operaciones) {
		this.operaciones = operaciones;
	}

}
