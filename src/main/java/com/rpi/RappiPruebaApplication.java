package com.rpi;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.env.StandardEnvironment;

import com.ulisesbocchio.jasyptspringboot.environment.EncryptableEnvironment;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class RappiPruebaApplication {

	public static void main(String[] args) {

		new SpringApplicationBuilder().environment(new EncryptableEnvironment(new StandardEnvironment()))
				.sources(RappiPruebaApplication.class).run(args);
	}

}
