package com.rpi.ws;

import com.rpi.api.OperacionCubo;
import com.rpi.api.ResultadoOperacion;

public interface WSOperacionCubo3D {
	String URL_OPERAR_CUBO = "/operarCubo";

	ResultadoOperacion traerResultadoOperaciones(OperacionCubo operacionTotal);
}
