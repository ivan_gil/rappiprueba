package com.rpi.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rpi.api.OperacionCubo;
import com.rpi.api.ResultadoOperacion;
import com.rpi.mvc.control.CuboServiceController;

@Controller
@RequestMapping("cubo3DWS")
public class WSOperacionCubo3DImpl implements WSOperacionCubo3D {

	@Autowired
	CuboServiceController cuboService;

	@Override

	@RequestMapping(value = URL_OPERAR_CUBO, method = RequestMethod.POST, consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody ResultadoOperacion traerResultadoOperaciones(@RequestBody OperacionCubo operacionTotal) {

		return cuboService.traerResultadoOperaciones(operacionTotal);
	}

}
