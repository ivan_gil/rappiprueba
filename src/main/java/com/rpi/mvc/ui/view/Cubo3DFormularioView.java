package com.rpi.mvc.ui.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.rpi.api.CfgField;
import com.rpi.api.Operacion;
import com.rpi.api.OperacionCubo;
import com.rpi.api.ResultadoOperacion;
import com.rpi.api.TipoOperacion;
import com.rpi.mvc.control.CuboServiceController;
import com.rpi.mvc.ui.tools.GeneradorCampos;
import com.rpi.mvc.ui.tools.IntegerField;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Select;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("deprecation")
@SpringUI
@Scope("prototype")
public class Cubo3DFormularioView extends VerticalLayout implements View {

	private static final long serialVersionUID = -8371079936514402032L;
	@Autowired
	private GeneradorCampos generadorCampos;
	@Autowired
	private CuboServiceController cuboService;

	TabSheet panelTests = new TabSheet(), panelResultado = new TabSheet();
	VerticalLayout layoutResultados = new VerticalLayout();
	List<TipoOperacion> tipos = new ArrayList<TipoOperacion>();

	@PostConstruct
	public void init() {
		setMargin(true);
		setSizeFull();
		setHeight(-1, Unit.PIXELS);
		VerticalLayout css = new VerticalLayout();

		css.setSizeFull();
		css.setHeight(-1, Unit.PIXELS);
		css.addComponent(new Label("Formulario Operacion CUBO3D"));
		CfgField cfgField = new CfgField();
		cfgField.setCaption("Numero de Tests");
		cfgField.setId(1l);
		cfgField.setName("numeroTest");
		cfgField.setPrompt("Digite");
		cfgField.setTooltip("Numero de Test");
		IntegerField numeroFormularios = (IntegerField) generadorCampos.createFieldNumber(cfgField);

		numeroFormularios.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 5747290681968685778L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				panelTests.removeAllComponents();
				for (int i = 0; i < new Integer(event.getProperty().getValue() + ""); i++) {
					panelTests.addTab(crearPanel(), "Test " + (i + 1));
				}

			}

		});

		css.addComponent(numeroFormularios);
		numeroFormularios.setSizeFull();

		tipos.add(new TipoOperacion(1, "UPDATE"));
		tipos.add(new TipoOperacion(2, "QUERY"));
		css.addComponent(panelTests);
		panelResultado.addTab(layoutResultados, "Resultados Input ");
		css.addComponent(panelResultado);
		addComponent(css);
		setComponentAlignment(css, Alignment.MIDDLE_CENTER);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {

	}

	public VerticalLayout crearPanel() {
		FieldGroup binder;
		PropertysetItem item = new PropertysetItem();
		binder = new FieldGroup(item);
		Button ejecutarBoton = new Button("Ejecutar ");
		ejecutarBoton.addStyleName(ValoTheme.BUTTON_PRIMARY);

		HorizontalLayout layoutH = new HorizontalLayout();
		VerticalLayout layoutV = new VerticalLayout();
		CfgField cfgField = new CfgField();
		cfgField.setCaption("Tamaño Matriz");
		cfgField.setId(1l);
		cfgField.setName("tamanoMatriz");
		cfgField.setPrompt("Digite");
		cfgField.setTooltip("Tamaño Matriz");
		IntegerField tamanoMatriz = (IntegerField) generadorCampos.createFieldNumber(cfgField);
		CfgField cfgField1 = new CfgField();
		cfgField1.setCaption("Numero de operaciones");
		cfgField1.setId(1l);
		cfgField1.setName("numeroConsultas");
		cfgField1.setPrompt("Digite");
		cfgField1.setTooltip("Numero de operaciones");
		IntegerField numeroConsultas = (IntegerField) generadorCampos.createFieldNumber(cfgField1);
		ejecutarBoton.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1898L;

			@Override
			public void buttonClick(final ClickEvent event) {

				OperacionCubo operacionTotal = new OperacionCubo();
				operacionTotal.setNumeroConsultas(new Integer(numeroConsultas.getValue()));
				operacionTotal.setTamanoMatriz(new Integer(tamanoMatriz.getValue()));
				operacionTotal.setNumeroTests(4);
				List<Operacion> operaciones = new ArrayList<Operacion>();
				for (int m = 0; m < new Integer(numeroConsultas.getValue()); m++) {
					Operacion operacion = new Operacion();
					TipoOperacion tipo = ((TipoOperacion) binder.getField("tipo" + m).getValue());
					operacion.setTipo(tipo);

					String x = binder.getField("x_" + m).getValue().toString();
					if (x != null && !x.equals(""))
						operacion.setValorX(new Integer(x));
					String y = binder.getField("y_" + m).getValue().toString();
					if (y != null && !y.equals(""))
						operacion.setValorY(new Integer(y));
					String z = binder.getField("z_" + m).getValue().toString();
					if (z != null && !z.equals(""))
						operacion.setValorZ(new Integer(z));

					String x1 = binder.getField("x1_" + m).getValue().toString();
					if (x1 != null && !x1.equals(""))
						operacion.setValorX1(new Integer(x1));
					String y1 = binder.getField("y1_" + m).getValue().toString();
					if (y1 != null && !y1.equals(""))
						operacion.setValorY1(new Integer(y1));
					String z1 = binder.getField("z1_" + m).getValue().toString();
					if (z1 != null && !z1.equals(""))
						operacion.setValorZ1(new Integer(z1));
					operaciones.add(operacion);
				}
				operacionTotal.setOperaciones(operaciones);

				ResultadoOperacion resultado = cuboService.traerResultadoOperaciones(operacionTotal);
				System.out.println(resultado);
				if (resultado.getEstado() == 0) {
					for (String resultadoF : resultado.getResultadosOK())
						layoutResultados.addComponent(new Label(resultadoF));
				} else {
					layoutResultados.addComponent(new Label(resultado.getRespuesta()));
				}
			}
		});

		numeroConsultas.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 5747290681968685778L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				numeroConsultas.setReadOnly(true);

				for (int i = 0; i < new Integer(event.getProperty().getValue() + ""); i++) {
					ComboBox listaTipoOperacion = new ComboBox("Operacion");
					HorizontalLayout layoutH1 = new HorizontalLayout();
					HorizontalLayout layoutH2 = new HorizontalLayout();
					BeanItemContainer<TipoOperacion> container = new BeanItemContainer<>(TipoOperacion.class);
					container.addAll(tipos);
					listaTipoOperacion.setContainerDataSource(container);
					item.addItemProperty("tipo" + i, new ObjectProperty<TipoOperacion>(new TipoOperacion()));
					listaTipoOperacion.setId(i + "");
					binder.bind(listaTipoOperacion, "tipo" + i);

					listaTipoOperacion.setItemCaptionMode(Select.ITEM_CAPTION_MODE_PROPERTY);
					listaTipoOperacion.setItemCaptionPropertyId("descripcion");
					item.addItemProperty("x_" + listaTipoOperacion.getId(), new ObjectProperty<String>(""));
					TextField x = (TextField) generadorCampos
							.createFieldNumber(new CfgField(2l, "x", "X", "X", "Digite"));
					layoutH2.addComponent(x);
					binder.bind(x, "x_" + listaTipoOperacion.getId());
					item.addItemProperty("y_" + listaTipoOperacion.getId(), new ObjectProperty<String>(""));
					TextField y = (TextField) generadorCampos
							.createFieldNumber(new CfgField(2l, "y", "Y", "Y", "Digite"));
					layoutH2.addComponent(y);
					binder.bind(x, "y_" + listaTipoOperacion.getId());
					item.addItemProperty("z_" + listaTipoOperacion.getId(), new ObjectProperty<String>(""));
					TextField z = (TextField) generadorCampos
							.createFieldNumber(new CfgField(2l, "z", "Z", "Z", "Digite"));
					layoutH2.addComponent(z);
					binder.bind(z, "z_" + listaTipoOperacion.getId());
					item.addItemProperty("x1_" + listaTipoOperacion.getId(), new ObjectProperty<String>(""));
					TextField x1 = (TextField) generadorCampos
							.createFieldNumber(new CfgField(2l, "x1", "X1", "X1", "Digite"));
					layoutH2.addComponent(x1);
					binder.bind(x1, "x1_" + listaTipoOperacion.getId());
					item.addItemProperty("y1_" + listaTipoOperacion.getId(), new ObjectProperty<String>(""));
					TextField y1 = (TextField) generadorCampos
							.createFieldNumber(new CfgField(2l, "y1", "Y1", "Y1", "Digite"));
					layoutH2.addComponent(y1);
					binder.bind(y1, "y1_" + listaTipoOperacion.getId());
					item.addItemProperty("z1_" + listaTipoOperacion.getId(), new ObjectProperty<String>(""));
					TextField z1 = (TextField) generadorCampos
							.createFieldNumber(new CfgField(2l, "z1", "Z1", "Z1", "Digite"));
					layoutH2.addComponent(z1);
					binder.bind(z1, "z1_" + listaTipoOperacion.getId());

					x.setVisible(false);
					y.setVisible(false);
					z.setVisible(false);
					x1.setVisible(false);
					y1.setVisible(false);
					z1.setVisible(false);

					listaTipoOperacion.addValueChangeListener(new ValueChangeListener() {

						private static final long serialVersionUID = 5747290681968685778L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							TipoOperacion tipo = (TipoOperacion) listaTipoOperacion.getValue();
							listaTipoOperacion.getId();

							if (tipo != null && tipo.getId() == 1) {
								x1.setCaption("W");
								x1.setInputPrompt("W");
								x.setVisible(true);
								y.setVisible(true);
								z.setVisible(true);
								x1.setVisible(true);
								z1.setVisible(false);
								y1.setVisible(false);
							} else if (tipo != null && tipo.getId() == 2) {
								x1.setCaption("X1");
								x1.setInputPrompt("X1");
								x.setVisible(true);
								y.setVisible(true);
								z.setVisible(true);
								x1.setVisible(true);
								z1.setVisible(true);
								y1.setVisible(true);
							}

						}
					});
					layoutH1.addComponent(listaTipoOperacion);
					layoutH1.addComponent(layoutH2);
					layoutV.addComponent(layoutH1);
				}

				layoutV.removeComponent(ejecutarBoton);
				layoutV.addComponent(ejecutarBoton);

			}

		});

		layoutH.addComponents(tamanoMatriz, numeroConsultas);
		layoutV.addComponent(layoutH);

		return layoutV;
	}
}