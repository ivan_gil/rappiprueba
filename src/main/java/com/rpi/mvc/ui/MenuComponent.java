package com.rpi.mvc.ui;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.rpi.mvc.ui.tools.ValoMenuLayout;
import com.rpi.mvc.ui.view.Cubo3DFormularioView;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SpringUI
@Scope("prototype")
public class MenuComponent extends VerticalLayout {

	private static final long serialVersionUID = 4538144003304849207L;
	ValoMenuLayout root = new ValoMenuLayout();
	ComponentContainer viewDisplay = root.getContentContainer();
	CssLayout menu = new CssLayout();
	CssLayout menuItemsLayout = new CssLayout();

	{
		menu.setId("Menu");
	}

	@Autowired
	private Cubo3DFormularioView cuboView;

	private Navigator navigator;

	CssLayout buildMenu() {

		final HorizontalLayout top = new HorizontalLayout();
		top.setWidth("100%");
		top.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
		top.addStyleName("valo-menu-title");
		menu.addComponent(top);

		final Button showMenu = new Button("Menu", new ClickListener() {

			private static final long serialVersionUID = 8034432472759180939L;

			@Override
			public void buttonClick(final ClickEvent event) {
				if (menu.getStyleName().contains("valo-menu-visible")) {
					menu.removeStyleName("valo-menu-visible");
				} else {
					menu.addStyleName("valo-menu-visible");
				}
			}
		});
		showMenu.addStyleName(ValoTheme.BUTTON_PRIMARY);
		showMenu.addStyleName(ValoTheme.BUTTON_SMALL);
		showMenu.addStyleName("valo-menu-toggle");
		showMenu.setIcon(FontAwesome.LIST);
		menu.addComponent(showMenu);
		ThemeResource resource = new ThemeResource("../tests-valo/logo/rappi.png");
		Image image = new Image(null, resource);

		image.setHeight("60px");
		image.setWidth("100px");
		top.addComponent(image);
		top.setExpandRatio(image, 1);

		final MenuBar settings = new MenuBar();
		settings.addStyleName("user-menu");
		final MenuItem settingsItem = settings.addItem(" Rappi ",
				new ThemeResource("../tests-valo/img/profile-pic-300px.jpg"), null);
		settingsItem.addSeparator();

		menu.addComponent(settings);

		menuItemsLayout.setPrimaryStyleName("valo-menuitems");
		Button b = new Button(" Operacion CUBO 3D", new ClickListener() {

			private static final long serialVersionUID = 6857327502542140316L;

			@Override
			public void buttonClick(final ClickEvent event) {
				navigator.navigateTo("operacion");

			}
		});

		b.setPrimaryStyleName("valo-menu-item");

		b.setSizeFull();
		b.setHeightUndefined();
		b.setCaption("Operacion CUBO 3D ");
		menuItemsLayout.addComponent(b);
		menu.addComponent(menuItemsLayout);

		menu.setSizeFull();
		menu.setHeightUndefined();
		menuItemsLayout.setSizeFull();
		menuItemsLayout.setHeightUndefined();

		return menu;
	}

	public void generarMenu() {

		root.setWidth("100%");

		menu.removeAllComponents();
		menuItemsLayout.removeAllComponents();

		root.addMenu(buildMenu());

		navigator = new Navigator(UI.getCurrent(), viewDisplay);
		navigator.addView("operacion", cuboView);

		menu.addComponent(menuItemsLayout);

		final String f = Page.getCurrent().getUriFragment();
		if (f == null || f.equals("")) {
			navigator.navigateTo("operacion");
		}

		navigator.addViewChangeListener(new ViewChangeListener() {

			private static final long serialVersionUID = 323560534808121117L;

			@Override
			public boolean beforeViewChange(final ViewChangeEvent event) {
				return true;
			}

			@Override
			public void afterViewChange(final ViewChangeEvent event) {
				for (final Iterator<Component> it = menuItemsLayout.iterator(); it.hasNext();) {
					it.next().removeStyleName("selected");
				}

				menu.removeStyleName("valo-menu-visible");
			}
		});

	}

}
