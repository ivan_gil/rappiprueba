package com.rpi.mvc.ui;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.CustomizedSystemMessages;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.SystemMessagesProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.server.SpringVaadinServlet;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SpringUI
@Theme("tests-valo-metro")
@Title("Rappi Prueba Cubo 3D")
@PreserveOnRefresh
@Widgetset("com.waysistem.gwt.SicovaWidgetset")
public class PrincipalUI extends UI {

	private static final long serialVersionUID = 3026186409654142563L;

	@Autowired
	public LoginComponent loginV;

	@Autowired
	public MenuComponent menu;

	VerticalLayout contentLayout = new VerticalLayout();

	@WebServlet(value = "/ui/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = true, ui = PrincipalUI.class, widgetset = "com.waysistem.gwt.SicovaWidgetset")
	public static class Servlet extends SpringVaadinServlet {

		private static final long serialVersionUID = -4681018896542679158L;

		@Override
		protected void servletInitialized() throws ServletException {
			super.servletInitialized();
			getService().setSystemMessagesProvider((SystemMessagesProvider) systemMessagesInfo -> {
				CustomizedSystemMessages messages = new CustomizedSystemMessages();
				messages.setSessionExpiredNotificationEnabled(false);
				messages.setCommunicationErrorNotificationEnabled(false);
				return messages;
			});
			getService().addSessionInitListener(new ValoThemeSessionInitListener());
		}
	}

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		if (getPage().getWebBrowser().isIE() && getPage().getWebBrowser().getBrowserMajorVersion() == 9) {
			menu.menu.setWidth("320px");
		}
		addStyleName(ValoTheme.UI_WITH_MENU);
		menu.menu.removeAllComponents();
		menu.menuItemsLayout.removeAllComponents();

		Responsive.makeResponsive(this);
		getPage().setTitle("Rappi Prueba - CUBO 3D");

		setErrorHandler(this::handleError);

		generarLogin();

	}

	public void generarLogin() {

		setContent(loginV);
		addStyleName("loginview");
		loginV.show(Page.getCurrent());
		loginV.setPortadaUI(this);
	}

	private void handleError(com.vaadin.server.ErrorEvent event) {
		Throwable t = DefaultErrorHandler.findRelevantThrowable(event.getThrowable());
		if (t instanceof AccessDeniedException) {
			Notification.show("No tienes permisos para esta operacion", Notification.Type.WARNING_MESSAGE);
		} else {
			DefaultErrorHandler.doDefault(event);
		}
	}

	public void pushSesion() {
		getUI().getPushConfiguration().setTransport(Transport.WEBSOCKET);
		getUI().getPushConfiguration().setPushMode(PushMode.AUTOMATIC);
	}

	public void showMenu() {
		menu.generarMenu();
		setContent(menu.root);

	}

	public void cerrarSesion() {
		getPage().reload();
		getSession().close();
	}

}