package com.rpi.mvc.ui.tools;

import com.vaadin.ui.Window;

public class Modal extends Window {

	private static final long serialVersionUID = 7081275279897454018L;

	public Modal(String caption) {
		super(caption);
		center();
		this.setResizeLazy(true);
		this.setResizable(false);

	}

}