package com.rpi.mvc.ui.tools;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;

import org.springframework.context.annotation.Scope;

import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.spring.annotation.SpringUI;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author Ivan Gil
 */
@SpringUI
@Scope("prototype")
public class GeneradorPDF {

	private JRDataSource datasource;

	public StreamSource generar(String reporteFile, Map<String, Object> parametrosReporte, Boolean isLocal) {

		StreamResource.StreamSource source = null;

		source = new StreamResource.StreamSource() {

			private static final long serialVersionUID = -7858826706921586253L;

			@Override
			public InputStream getStream() {
				byte[] byteFile = null;
				try {
					byteFile = JasperRunManager.runReportToPdf(
							getClass().getClassLoader().getResourceAsStream(reporteFile), parametrosReporte,
							datasource);

				} catch (JRException ex) {
					ex.printStackTrace();
				}

				return new ByteArrayInputStream(byteFile);
			}

		};
		return source;

	}

	public StreamSource procesaFile(final File file) {

		StreamSource source = new StreamSource() {
			private static final long serialVersionUID = -7775299715708693982L;

			@Override
			public java.io.InputStream getStream() {
				try {

					FileInputStream helpFileInputStream = new FileInputStream(file);
					return helpFileInputStream;
				} catch (FileNotFoundException e) {

					e.printStackTrace();
				}
				return null;
			}
		};
		return source;

	}

	public JRDataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(JRDataSource datasource) {
		this.datasource = datasource;
	}

}
