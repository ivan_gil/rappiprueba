/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rpi.mvc.ui.tools;

import java.io.Serializable;

import com.rpi.api.CfgField;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Field;

/**
 *
 * @author Ivan Alberto Gil
 */
@SpringUI
public class GeneradorCampos implements Serializable {

	private static final long serialVersionUID = 1L;

	public GeneradorCampos() {

	}

	@SuppressWarnings("rawtypes")
	public Field createFieldNumber(CfgField field) {

		IntegerField numberField = new IntegerField();

		numberField.setCaption(field.getCaption());
		numberField.setId(field.getName());
		numberField.setInputPrompt(field.getTooltip());
		numberField.setImmediate(true);

		return numberField;

	}

}
