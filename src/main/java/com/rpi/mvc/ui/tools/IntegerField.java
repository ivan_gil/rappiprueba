package com.rpi.mvc.ui.tools;

import java.math.BigInteger;

import com.vaadin.data.Validator;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.TextField;

public class IntegerField extends TextField implements TextChangeListener {

	private static final long serialVersionUID = -8021279372844022559L;
	String lastValue;

	public IntegerField() {
		setImmediate(true);
		setTextChangeEventMode(TextChangeEventMode.EAGER);
		addTextChangeListener(this);
		addValidator(new ValidadorNumero());
	}

	@Override
	public void textChange(TextChangeEvent event) {
		String text = new String(event.getText() + "");
		try {
			if (text != null && !text.equals(""))
				new BigInteger(text);
			lastValue = text;
		} catch (NumberFormatException e) {
			if (text == null || text == "")
				setValue("");
			else
				setValue(lastValue);

		}
	}

	private class ValidadorNumero implements Validator {

		private static final long serialVersionUID = -8311108248649179311L;

		@Override
		public void validate(Object value) throws InvalidValueException {
			String text = new String(value + "");
			try {
				if (text != null && text.toString() != null && !text.equals(""))
					new BigInteger(text.toString());

			} catch (NumberFormatException e) {
				setValue("");
				throw new InvalidValueException("valor numerico invalido");

			}
		}
	}
}