package com.rpi.mvc.control;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.rpi.api.Constantes;
import com.rpi.api.Cubo3D;
import com.rpi.api.Operacion;
import com.rpi.api.OperacionCubo;
import com.rpi.api.ResultadoOperacion;
import com.rpi.mvc.model.RappiPruebaDAO;
import com.vaadin.spring.annotation.SpringComponent;

@SpringComponent
public class CuboServiceControllerBean implements CuboServiceController {

	@Autowired
	RappiPruebaDAO dao;

	Cubo3D cubo = new Cubo3D();

	@Override
	public ResultadoOperacion traerResultadoOperaciones(OperacionCubo operacionTotal) {
		ResultadoOperacion resultadoValidacion = validarOperaciones(operacionTotal);
		if (resultadoValidacion.getEstado() == 0) {

			List<String> resultados = new ArrayList<String>();
			cubo.setTamanoMatriz(operacionTotal.getTamanoMatriz());

			for (Operacion operacion : operacionTotal.getOperaciones()) {
				if (operacion.getTipo().getId() == 1) {
					cubo = dao.actualizarCubo3D(cubo, operacion);

				} else {
					String resultado = dao.consultarCubo3D(cubo, operacion);
					resultados.add(resultado);
				}

			}
			resultadoValidacion.setResultadosOK(resultados);
		}
		return resultadoValidacion;
	}

	public ResultadoOperacion validarOperaciones(OperacionCubo operacionTotal) {
		String mensajesError = "";
		ResultadoOperacion resultado = new ResultadoOperacion();
		resultado.setEstado(0);
		if (operacionTotal.getNumeroTests() < 1
				|| operacionTotal.getNumeroTests() > Constantes.NUMERO_MAX_TEST_PERMITIDO) {
			mensajesError += "Numero de tests fuera del rango permitido [1 al " + Constantes.NUMERO_MIN_VALOR_PERMITIDO
					+ "]";
		}
		if (operacionTotal.getNumeroConsultas() < 1
				|| operacionTotal.getNumeroConsultas() > Constantes.NUMERO_MAX_CONSULTAS_PERMITIDO) {
			mensajesError += "Numero de consultas fuera del rango permitido [1 al "
					+ Constantes.NUMERO_MAX_CONSULTAS_PERMITIDO + "]";
		}
		if (operacionTotal.getTamanoMatriz() < 1
				|| operacionTotal.getTamanoMatriz() > Constantes.NUMERO_MAX_TAMANO_PERMITIDO) {
			mensajesError += "Tamaño de Matriz Cubo fuera del rango permitido [1 al "
					+ Constantes.NUMERO_MAX_TAMANO_PERMITIDO + "]";
		}
		if (operacionTotal.getOperaciones() == null
				|| operacionTotal.getOperaciones().size() < operacionTotal.getNumeroConsultas()) {
			mensajesError += "No se han informado las operaciones";
		} else {
			for (Operacion operacion : operacionTotal.getOperaciones()) {
				if (operacion.getTipo().getId() == 1) {
					if (operacion.getValorX() < 1 || operacion.getValorX() > operacionTotal.getTamanoMatriz()) {
						mensajesError += "Fuera del rango establecido para el valor de X";
					} else if (operacion.getValorY() < 1 || operacion.getValorY() > operacionTotal.getTamanoMatriz()) {
						mensajesError += "Fuera del rango establecido para el valor de Y";
					} else if (operacion.getValorZ() < 1 || operacion.getValorZ() > operacionTotal.getTamanoMatriz()) {
						mensajesError += "Fuera del rango establecido para el valor de Z";
					} else if (operacion.getValorX1() < Constantes.NUMERO_MIN_VALOR_PERMITIDO
							|| operacion.getValorX1() > Constantes.NUMERO_MAX_VALOR_PERMITIDO) {
						mensajesError += "Valor a asignar a la celda (" + operacion.getValorX() + ", "
								+ operacion.getValorY() + ", " + operacion.getValorZ()
								+ ") fuera del rango -109 al 109";
					}

				} else {
					System.out.println(operacion);
					if (operacion.getValorX() < 1 || operacion.getValorX() > operacionTotal.getTamanoMatriz()) {
						mensajesError += "Fuera del rango establecido para el valor de X";
					} else if (operacion.getValorY() < 1 || operacion.getValorY() > operacionTotal.getTamanoMatriz()) {
						mensajesError += "Fuera del rango establecido para el valor de Y";
					} else if (operacion.getValorZ() < 1 || operacion.getValorZ() > operacionTotal.getTamanoMatriz()) {
						mensajesError += "Fuera del rango establecido para el valor de Z";
					} else if (operacion.getValorX() > operacion.getValorX1()
							|| operacionTotal.getTamanoMatriz() < operacion.getValorX1()) {
						mensajesError += "Fuera del rango establecido para el valor de X1";
					} else if (operacion.getValorY1() < operacion.getValorY()
							|| operacion.getValorY1() > operacionTotal.getTamanoMatriz()) {
						mensajesError += "Fuera del rango establecido para el valor de Y1";
					} else if (operacion.getValorZ1() < operacion.getValorZ()
							|| operacion.getValorZ1() > operacionTotal.getTamanoMatriz()) {
						mensajesError += "Fuera del rango establecido para el valor de Z1";
					}
				}

			}
		}
		if (!mensajesError.equals("")) {
			resultado.setEstado(-1);
			resultado.setRespuesta("Correguir errores encontrados :" + mensajesError);

		}

		return resultado;
	}

}
