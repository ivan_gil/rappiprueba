package com.rpi.mvc.control;

import com.rpi.api.OperacionCubo;
import com.rpi.api.ResultadoOperacion;

public interface CuboServiceController {

	ResultadoOperacion traerResultadoOperaciones(OperacionCubo operacionTotal);

}
