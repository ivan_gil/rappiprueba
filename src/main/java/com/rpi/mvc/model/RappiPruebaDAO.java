package com.rpi.mvc.model;

import org.springframework.stereotype.Repository;

import com.rpi.api.Cubo3D;
import com.rpi.api.Operacion;

@Repository
public class RappiPruebaDAO {

	public String consultarCubo3D(Cubo3D cubo, Operacion operacion) {
		int resultado = 0, z = 0;
		for (int i = operacion.getValorX(); i <= operacion.getValorX1(); i++) {
			for (int j = operacion.getValorY(); j <= operacion.getValorY1(); j++) {
				for (int k = operacion.getValorZ(); k <= operacion.getValorZ1(); k++) {
					resultado += cubo.getBloques()[i][j][k];
					System.out.println(" PROCESO ............[" + i + ", " + j + ", " + k + "]\n");
				}

			}
		}

		return resultado + "";
	}

	public Cubo3D actualizarCubo3D(Cubo3D cubo, Operacion operacion) {
		cubo.getBloques()[operacion.getValorX()][operacion.getValorY()][operacion.getValorZ()] = operacion.getValorX1();
		return cubo;
	}

	public int sumarFaltantes(int i, int j, int k) {

		return 0;
	}

}
